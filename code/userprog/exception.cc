// exception.cc
//	Entry point into the Nachos kernel from user programs.
//	There are two kinds of things that can cause control to
//	transfer back to here from user code:
//
//	syscall -- The user code explicitly requests to call a procedure
//	in the Nachos kernel.  Right now, the only function we support is
//	"Halt".
//
//	exceptions -- The user code does something that the CPU can't handle.
//	For instance, accessing memory that doesn't exist, arithmetic errors,
//	etc.
//
//	Interrupts (which can also cause control to transfer from user
//	code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include <stdio.h>        // FA98
#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include "addrspace.h"   // FA98
#include "sysdep.h"   // FA98

// begin FA98

static int SRead(int addr, int size, int id);
static void SWrite(char *buffer, int size, int id);

// end FA98
void
dummy(int arg)
{
	currentThread->Yield();
	currentThread->space->InitRegisters();
	currentThread->space->RestoreState();

	machine->Run();
	ASSERT(FALSE);
}


//----------------------------------------------------------------------
// ExceptionHandler
// 	Entry point into the Nachos kernel.  Called when a user program
//	is executing, and either does a syscall, or generates an addressing
//	or arithmetic exception.
//
// 	For system calls, the following is the calling convention:
//
// 	system call code -- r2
//		arg1 -- r4
//		arg2 -- r5
//		arg3 -- r6
//		arg4 -- r7
//
//	The result of the system call, if any, must be put back into r2.
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//	"which" is the kind of exception.  The list of possible exceptions
//	are in machine.h.
//----------------------------------------------------------------------

void
ExceptionHandler(ExceptionType which)
{
	int type = machine->ReadRegister(2);

	int arg1 = machine->ReadRegister(4);
	int arg2 = machine->ReadRegister(5);
	int arg3 = machine->ReadRegister(6);
	int Result;
	int i, j;
	char *ch = new char [500];

	switch ( which )
	{
	case NoException :
		break;
	case SyscallException :

		// for debugging, in case we are jumping into lala-land
		// Advance program counters.
		machine->registers[PrevPCReg] = machine->registers[PCReg];
		machine->registers[PCReg] = machine->registers[NextPCReg];
		machine->registers[NextPCReg] = machine->registers[NextPCReg] + 4;
		switch ( type )
		{

		case SC_Halt :
		{
			printf("\nSC_HALT is called by %s \n", currentThread->getName());
			DEBUG('t', "Shutdown, initiated by user program.\n");
			interrupt->Halt();
			break;
		}

			case SC_Exec:
			printf("\nSC_Exec is called by %s \n", currentThread->getName());
									{
									int var;
									int buffadd = machine->ReadRegister(4);
									char *filename = new char[100];
									if (!machine ->ReadMem(buffadd,1,&var)) return;
									i = 0;
									while (var!=0)
									{
										if (i >= 99 ){
											printf("File name is too big HOMEBOI!\n");
											break;
										}
											filename[i] = (char)var;
											buffadd+=1;
											i++;
											if (!machine->ReadMem(buffadd,1,&var)) return;
									}

									filename[i] = (char)0;

									// for (size_t i = 0; i < 100; i++) {
									// 	if (filename == filenameArray[i].filename){
									// 		filenameArray[i].counter += 1;
									// 		if (filenameArray[i].counter > 3){
									// 			Index = i;
									// 			printf("You have called a file 3 times. Exiting processes to make room! \n");
									// 			kickout = true;
									// 		}
									//
									//
									// 		break;
									// 	}
									// 	if (filenameArray[i].filename == NULL){
									// 		filenameArray[i].filename = filename;
									// 	}
									// }
									// if (kickout){
									// 	goto Exit;
									// }
									printf("Filename: %s\n",filename );
									exiting = false;
									OpenFile *executable = fileSystem->Open(filename);
									if (executable != NULL){
									AddrSpace *space;
									space = new AddrSpace(executable);
									if (exiting){
										goto End;
									}
									Thread *t = new Thread("");
									t->ParentThread = currentThread;
									printf("%s has id %d\n",t->getName(), t->getID());
									t->space = space;
									int processId = t->getID();
									int pc;
									pc=machine->ReadRegister(PCReg);
									machine->WriteRegister(PrevPCReg,pc);
									pc=machine->ReadRegister(NextPCReg);
									pc+=4;
									machine->WriteRegister(NextPCReg,pc);
									t->Fork(dummy,t->getID());
									machine->WriteRegister(2,processId);
								}
								else {
									printf("Could not find file homeboi!\n");
									exiting = true;
								}
								End: ;
									break;
								}

		 case SC_Join:
		 printf("\nSC_Join is called by %s \n", currentThread->getName());
									{
										if (!exiting){
											printf("Hello!!1\n");
									IntStatus oldLevel = interrupt->SetLevel(IntOff);
									currentThread->Sleep();

									(void) interrupt->SetLevel(oldLevel);
}
									break;
								}

		case SC_Exit:
		printf("\nSC_Exit is called by %s \n", currentThread->getName());
					{
								// Exit: ;
								// if (kickout){
								// 	filenameArray[Index].counter--;
								// }
								// if (kickout && filenameArray[Index].counter == 1){
								// 	kickout =false;
								// }
								for (int i = 0; i < currentThread->space->getPages()+1; i++ )
								{
									globalMap->Clear((currentThread->space->firstPage) + i);
								}
								bitmapMark = globalMap->NumClear();
								//globalMap->Print();
								printf("Thread %d exited with code %d\n", currentThread->getID(), arg1);
								//currentThread->space->~AddrSpace();
								IDcount -=1;
								if (IDcount != 0){
									IntStatus oldLevel = interrupt->SetLevel(IntOff);

									//Thread *parent;
								//	parent = (Thread) threadArray[0];
									//printf("%d\n", threadArray[0]->getID());
								scheduler->ReadyToRun(currentThread->ParentThread);
								printf("%s is parent\n", currentThread->ParentThread->getName());
									(void) interrupt->SetLevel(oldLevel);
//
//
}
globalMap->Print();

								currentThread->Finish();
								interrupt->Halt();
								printf("Hello\n");
									break;

	}
							case SC_Yield:
							printf("\nSC_Yield is called by %s \n", currentThread->getName());
									{
									currentThread->Yield();
									break;
								}

		case SC_Read :
		printf("\nSC_Read is called by %s \n", currentThread->getName());
			if (arg2 <= 0 || arg3 < 0){
				printf("\nRead 0 byte.\n");
			}
			Result = SRead(arg1, arg2, arg3);
			machine->WriteRegister(2, Result);
			DEBUG('t',"Read %d bytes from the open file(OpenFileId is %d)",
			arg2, arg3);
			break;

		case SC_Write :
		printf("\nSC_Write is called by %s \n", currentThread->getName());
			for (j = 0; ; j++) {
				if(!machine->ReadMem((arg1+j), 1, &i))
					j=j-1;
				else{
					ch[j] = (char) i;
					if (ch[j] == '\0')
						break;
				}
			}
			if (j == 0){
				printf("\nWrite 0 byte.\n");
				// SExit(1);
			} else {
				DEBUG('t', "\nWrite %d bytes from %s to the open file(OpenFileId is %d).", arg2, ch, arg3);
				SWrite(ch, j, arg3);
			}
			break;

			default:
				break;
		}

	case ReadOnlyException :
		//puts ("ReadOnlyException");
		if (currentThread->getName() == "main")
		ASSERT(FALSE);  //Not the way of handling an exception.
		//SExit(1);
		break;
	case BusErrorException :
		puts ("BusErrorException");
		if (currentThread->getName() == "main")
		ASSERT(FALSE);  //Not the way of handling an exception.
		//SExit(1);
		break;
	case AddressErrorException :
		puts ("Pointer out of bounds");
		currentThread->Finish();
		if (currentThread->getName() == "main")
		ASSERT(FALSE);  //Not the way of handling an exception.
		//SExit(1);
		break;
	case OverflowException :
		puts ("OverflowException");
		if (currentThread->getName() == "main")
		ASSERT(FALSE);  //Not the way of handling an exception.
		//SExit(1);
		break;
	case IllegalInstrException :
		puts ("IllegalInstrException");
		if (currentThread->getName() == "main")
		ASSERT(FALSE);  //Not the way of handling an exception.
		//SExit(1);
		break;
	case NumExceptionTypes :
		puts ("NumExceptionTypes");
		if (currentThread->getName() == "main")
		ASSERT(FALSE);  //Not the way of handling an exception.
		//SExit(1);
		break;

		default :
		//      printf("Unexpected user mode exception %d %d\n", which, type);
		//      if (currentThread->getName() == "main")
		//      ASSERT(FALSE);
		//      SExit(1);
		break;
	}
	delete [] ch;
}


static int SRead(int addr, int size, int id)  //input 0  output 1
{
	char buffer[size+10];
	int num,Result;

	//read from keyboard, try writing your own code using console class.
	if (id == 0)
	{
		scanf("%s",buffer);

		num=strlen(buffer);
		if(num>(size+1)) {

			buffer[size+1] = '\0';
			Result = size+1;
		}
		else {
			buffer[num+1]='\0';
			Result = num + 1;
		}

		for (num=0; num<Result; num++)
		{  machine->WriteMem((addr+num), 1, (int) buffer[num]);
			if (buffer[num] == '\0')
			break; }
		return num;

	}
	//read from a unix file, later you need change to nachos file system.
	else
	{
		for(num=0;num<size;num++){
			Read(id,&buffer[num],1);
			machine->WriteMem((addr+num), 1, (int) buffer[num]);
			if(buffer[num]=='\0') break;
		}
		return num;
	}
}


static void SWrite(char *buffer, int size, int id)
{

	//write to terminal, try writting your own code using console class.
	if (id == 1)
	printf("%s", buffer);
	//write to a unix file, later you need change to nachos file system.
	if (id >= 2)
	WriteFile(id,buffer,size);
}

// end FA98
