// threadtest.cc 
//	Simple test case for the threads assignment.
//
//	Create two threads, and have them context switch
//	back and forth between themselves by calling Thread::Yield, 
//	to illustratethe inner workings of the thread system.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include "synch.h"
#include "math.h"

using namespace std;

int numS;
int numT;

//----------------------------------------------------------------------
// SimpleThread
// 	Loop 5 times, yielding the CPU to another ready thread 
//	each iteration.
//
//	"which" is simply a number identifying the thread, for debugging
//	purposes.
//----------------------------------------------------------------------

void
SimpleThread(int which)
{
    int num;
    
    for (num = 0; num < 5; num++) {
	printf("*** thread %d looped %d times\n", which, num);
        currentThread->Yield();
    }
}

//----------------------------------------------------------------------
// InputValidation
// 	Will ask user for input, then validate the input and return the type
//----------------------------------------------------------------------

void
InputValidation(int which)
{
    //int go = 1;
    //while(go = 1){
        start: ;
        char input [200];
        char inType[200];
        int decimal = 0;
        printf("Please enter anything: ");
        scanf("%s",input);
        
        for (size_t i = 0; i < strlen(input); i++)
        {
            if(input[i] == '-'){
                for (int j = 1; j < strlen(input); j++)
                {
                    if(isalpha(input[j])){
                        strcpy(inType,"Character Array");
                        goto last;
                    }
                    else if (isdigit(input[j]))
                    {
                        continue;
                    }
                    else if ((input[j]=='.')&&(decimal == 0)){
                        decimal++;
                        continue;
                    } 
                    else if ((input[j]=='.')&&(decimal == 1)){
                        strcpy(inType,"Character Array");
                        goto last;
                    } 
                    else{
                        strcpy(inType,"Character Array");
                        goto last;
                    }
                }
                if((decimal == 1)&&(strlen(input) > 2)&&(input[strlen(input)-1] != '.')){
                    strcpy(inType,"Negative Decimal");
                    goto last;
                }
                else if ((decimal == 1)&&(strlen(input) == 2))
                {
                    strcpy(inType,"Character Array");
                    goto last;
                }
                else
                {
                    strcpy(inType,"Negative Integer");
                    goto last;
                }
            }
            else if (isdigit(input[i]))
            {

                    for (int k = 1; k < strlen(input); k++)
                    {
                        if ((input[k]=='.')&&(decimal == 0)){
                            decimal++;
                            continue;
                        } 
                        else if ((input[k]=='.')&&(decimal == 1)){
                            strcpy(inType,"Character Array");
                            goto last;
                        } 
                        
                        else if(isalpha(input[k])){
                            strcpy(inType,"Character Array");
                            goto last;
                        }
                        else if (isdigit(input[i]))
                        {
                            continue;
                        }
                        
                        else{
                            strcpy(inType,"Character Array");
                            goto last;
                        }
                    }

                    if(decimal == 0){
                        strcpy(inType,"Positive Integer");
                        goto last;
                    }
                    else
                    {
                        strcpy(inType,"Positive Decimal");
                        goto last;
                    }
                }
            
            else if (input[0]=='.')
            {
                for (size_t i = 1; i < strlen(input); i++)
                {
                    if (!isdigit(input[i]))
                    {
                        strcpy(inType,"Character Array");
                        goto last;
                    }
                    else
                    {
                        strcpy(inType,"Positive Decimal");
                        goto last;
                    }
                } 
                strcpy(inType,"Positive Decimal");
                goto last;
            }
            
            else
            {
                continue;
            }
        }
         
        if (strlen(input) == 1)
        {
            strcpy(inType,"Character");
            goto last;
        }
        else
        {
            strcpy(inType,"Character Array");
            goto last;
        }
        
        last: ;
        printf("%s is a %s.",input, inType);
        printf("\r\n");
        
        /* 
        Loop: ;
        printf("\r\n");
        printf("Continue? (y/n): ");
        char choice = 'o';
        scanf("%c", choice);
            if(choice = 'y' || 'Y'){
                goto start;
            }
            else if(choice = 'n' || 'N'){
                
            }
            else
            {
                printf("Invalid decision");
                goto Loop;
            }
            */
}

//----------------------------------------------------------------------
// Shout
// 	Thread will take two integers, one for threads and one for number of shouts
//  and it would randomly select a shout.
//----------------------------------------------------------------------

void
Shout(int thread)
{
    char* shoutList [] = {"ding dong","yoink","Everyone dies, some just need a little help.",
    "By the light","yaga"};

    for (size_t i = 0; i < numS; i++)
    {
        printf("Thread: %d shouts- %s | Thread has shouted %d times.\n",thread, shoutList[Random()%5], i+1);
        
        int waiter = Random()%7;
        while ((waiter < 3))
        {
            waiter = Random()%7;
        }
        
        int j = 0;
        while (j<waiter)
        {
            j++;
            currentThread->Yield();
        }
         
    }
    currentThread->Finish();
}
    
//----------------------------------------------------------------------
// ShoutTest
// 	Thread will ask user for number of threads and the created threads
//  will randomly shout lines.
//----------------------------------------------------------------------

void
ShoutTest(int which)
{
    char numThread [1000];
    char numShouts [1000];

    again: ;
    printf("How many threads? (1-1000): ");
    scanf("%s",numThread);
    numT = atoi(numThread);
    if ((numT==0)||(numT<1)||(numT>1000))
    {
        printf("Invalid input try again: ");
        goto again;
    }
    
    again2: ;
    printf("How many shouts? (1-1000): ");
    scanf("%s",numShouts);
    numS = atoi(numShouts);
    if ((numS==0)||(numS<1)||(numS>1000))
    {
        printf("Invalid input try again: ");
        goto again2;
    }

    printf("%d", numS);

    
    for (size_t i = 0; i < numT; i++)
    {
        Thread  * tink  = new Thread("");
        tink ->Fork(Shout, i);
    }
/* 
    for (size_t i = 0; i < numT; i++)
    {
        tink[i]->Fork(Shout, i);
    }
    */
}

//----------------------------------------------------------------------
// Check
// Validation for user's to make sure an integer is entered
//
//----------------------------------------------------------------------

//Begin code written by David Duong

long check() {
     char  *input = new char[10000];

    for (int i =0; i <10000;i++){

        *(input + i) = NULL;

    }
    bool next = true;

    while(next){



    cin >> input;

    bool String = false;

    bool Integer = false;

    bool skip = false;

    if (*(input+1) == NULL){

        if (input[0] >='0' && input[0] <='9'){
            Integer = true;


        }


    skip = true;

    }

    for (int i = 0; i<10000; i++){

        if (skip || *(input+i) == NULL){

            break;

        }

        if (String){

            break;

        }

        if (*(input+i) >= '0' && *(input+i) <= '9'){

                Integer = true;

            }

            else {

                String = true;

            }



    }
if (Integer && !String){
break;
}
cout << "Value must be 0 or more. Please try again: ";
}

return atoi(input);

}

//End code written by David Duong

//----------------------------------------------------------------------
// Philospher Dining problem wihout semaphores
//
//----------------------------------------------------------------------

//Begin code written by David Duong

struct state {
bool ready = false;
int value;
  int remaining;
bool HUNGRY = false;
bool EATING = false;
bool THINKING = false;

};

state *philosphers;
bool *chopsticks;
int meals;
int numPhilosphers;
bool waiting = false;
int leaving =0;
void
pickUp(state philospher){

int id = philospher.value;
if (numPhilosphers ==1)
{
cout << "*******Philospher " << id << " tries to pick up chopstick #" << id<< endl;
chopsticks[1] = false;
cout << "********Philospher " << id << " picked up chopstick #" << 1<< endl;

}
else
{
if ((id % 2) ==1){
cout << "*****Philospher " << id << " tries to pick up chopstick #" << id << endl;
while(!chopsticks[id] || philosphers[(id+1)%numPhilosphers].EATING){
currentThread -> Yield();
}
chopsticks[id] = false;
cout << "******Philospher " << id << " picked up chopstick #" << id<< endl;

}
else{
cout << "*****Philospher " << id << " tries to pick up chopstick #" << ((id+1)%numPhilosphers)<< endl;
while(!chopsticks[((id+1)%numPhilosphers)] || philosphers[(id+numPhilosphers-1)%numPhilosphers].EATING){
currentThread -> Yield();
}
chopsticks[((id+1)%numPhilosphers)] = false;
cout << "******Philospher " << id << " picked up chopstick #" << ((id+1)%numPhilosphers)<< endl;
}
}
}
void
pickUpOther(state philospher){
int id = philospher.value;
if (numPhilosphers ==1)
{
cout << "*******Philospher " << id << " tries to pick up chopstick #" << id<< endl;
chopsticks[0] = false;
cout << "********Philospher " << id << " picked up chopstick #" << 0<< endl;

}
else {
if ((id) % 2 ==0){
cout << "*******Philospher " << id << " tries to pick up chopstick #" << id<< endl;
while(!chopsticks[id]  || philosphers[(id+1)%numPhilosphers].EATING){
currentThread -> Yield();
}
chopsticks[id] = false;
cout << "********Philospher " << id << " picked up chopstick #" << id<< endl;

}
else{
cout << "*******Philospher " << id << " tries to pick up chopstick #" << ((id+1)%numPhilosphers)<< endl;
while(!chopsticks[((id+1)%numPhilosphers)] || philosphers[(id+numPhilosphers-1)%numPhilosphers].EATING){
currentThread -> Yield();
}
chopsticks[((id+1)%numPhilosphers)] = false;
cout << "********Philospher " << id << " picked up chopstick #" << ((id+1)%numPhilosphers)<< endl;
}
}
}
void
putDown(state philospher){
int id = philospher.value;
if (numPhilosphers ==1){
chopsticks[1] =true;
cout << "**********Philospher " << id << " put down chopstick #" << 1<< endl;
}
else
{
if (id % 2 ==1){
chopsticks[id%numPhilosphers] = true;
cout << "**********Philospher " << id << " put down chopstick #" << id<< endl;

}
else{
chopsticks[((id+1)%numPhilosphers)] = true;
cout << "**********Philospher " << id << " put down chopstick #" << ((id+1)%numPhilosphers)<< endl;
}
}
}
void
putDownOther(state philospher){
int id = philospher.value;
if (numPhilosphers ==1){
chopsticks[0] =true;
cout << "**********Philospher " << id << " put down chopstick #" << 0<< endl;
}
else
{
if (id % 2 ==0){
chopsticks[id] = true;
cout << "***********Philospher " << id << " put down chopstick #" << id << endl;

}
else{
chopsticks[((id+1)%numPhilosphers)] = true;
cout << "***********Philospher " << id << " put down chopstick #" << ((id+1)%numPhilosphers)<< endl;
}
}
}
void
table(int id){
cout << "*Philospher " << id << " has entered the room. " << endl;
cout << "**Philospher " << id << " is waiting to sit at the table." <<endl;
philosphers[id].ready = true;
bool sat = false;
while (!sat){
int i =0;
while (i!=numPhilosphers){
sat = philosphers[i].ready;
i++;
}
currentThread -> Yield();
}
if (id == (numPhilosphers-1)){
cout << "***All philosphers has taken a seat!" << endl;
}
if (numPhilosphers == 1) {
while (meals !=0){
cout << "****Philospher " << id << " is hungry." << endl;
pickUp(philosphers[0]);
pickUpOther(philosphers[0]);
philosphers[0].HUNGRY = false;
meals--;
cout << "*********Philospher " << id << " is eating." << " There are " << meals << " meals left."<< endl;
for (int t = 0; t < (Random()%5+3);t++)
{
}
putDown(philosphers[id]);
putDownOther(philosphers[id]);
cout << "************Philospher " << id << " is done eating and is now thinking." << endl;
philosphers[id].THINKING =true;

for (int y = 0; y < (Random()%5+3);y++)
{ }

}
}
else
{
while (philosphers[id].remaining != 0){
philosphers[id].HUNGRY = true;

cout << "****Philospher " << id << " is hungry." << endl;

pickUp(philosphers[id]);
pickUpOther(philosphers[id]);
philosphers[id].HUNGRY = false;


meals+=1;
philosphers[id].remaining--;

cout << "*********Philospher " << id << " is eating." << " There are " << meals << " meals eaten."<< endl;


for (int t = 0; t < (Random()%5+3);t++)
{
currentThread -> Yield();
}
philosphers[id].EATING = false;
putDown(philosphers[id]);
putDownOther(philosphers[id]);
cout << "************Philospher " << id << " is done eating and is now thinking." << endl;
philosphers[id].THINKING =true;

for (int y = 0; y < (Random()%5+3);y++)
{
currentThread -> Yield();
}
philosphers[id].THINKING = false;

}
cout << "!!!!!!!!!Philospher " << id << " is waiting to leave." << endl;
}
leaving++;
if (leaving == numPhilosphers){
cout << "$$$$All philosphers have left the party!$$$" << endl;
}
while(leaving != numPhilosphers){
currentThread ->Yield();
}
}
void
diningPhilospher(int which){
cout << "How many philosphers will be at the dinner party? ";
numPhilosphers = check();
philosphers = new state[numPhilosphers];
cout << "How many meals will be available? ";
meals = check();
Thread *t[numPhilosphers];

if (numPhilosphers !=0){
chopsticks = new bool[numPhilosphers];
for (int i =0; i<numPhilosphers;i++){
t[i] = new Thread("");
chopsticks[i] = true;
philosphers[i].value = i;
      if (meals >0){
        philosphers[i].remaining++;
        meals--;
      }
}
}else{
chopsticks = new bool[2];
t[0] = new Thread("");
chopsticks[0] = true;
chopsticks[1] = true;

}
for (int i = 0; i < numPhilosphers; i++) {
t[i] -> Fork(table, i);
}

}

//End code written by David Duong

//----------------------------------------------------------------------
// Philospher Dining problem with semaphore
//
//
//----------------------------------------------------------------------

//Begin code written by David Duong and Randy Nguyen

Semaphore **chopsticksS;
Semaphore *wait = new Semaphore("Wait2Sit", 0);
Semaphore *staring = new Semaphore("Wait2Meal",1);
int count = 0;
void
pickUpS(state philospher)
{
  int id = philospher.value;

  if (numPhilosphers ==1)
  {
    cout << "*******Philospher " << id << " tries to pick up chopstick #" << id+1<< endl;
    (*(chopsticksS+1))->P();
    cout << "********Philospher " << id << " picked up chopstick #" << 1<< endl;

  }
  else
  {
    if ((id) % 2 ==1)
    {
      cout << "*******Philospher " << id << " tries to pick up chopstick #" << id<< endl;
      if (philosphers[(id+1)%numPhilosphers].EATING)
      {
        (*(chopsticksS+((id+1)%numPhilosphers)))->P();
        cout << "##########Avoided possible deadlock situation #######" << endl;

        (*(chopsticksS+((id+1)%numPhilosphers)))->V();

      }
      (*(chopsticksS+id))->P();

      cout << "********Philospher " << id << " picked up chopstick #" << id<< endl;

    }
    else{
      cout << "*******Philospher " << id << " tries to pick up chopstick #" << ((id+1)%numPhilosphers)<< endl;
      if (philosphers[(id+numPhilosphers-1)%numPhilosphers].EATING)
      {
        (*(chopsticksS+((id+numPhilosphers-1)%numPhilosphers)))->P();
        cout << "##########Avoided possible deadlock situation##########" << endl;

        (*(chopsticksS+((id+numPhilosphers-1)%numPhilosphers)))->V();

      }
      (*(chopsticksS+((id+1)%numPhilosphers)))->P();

      cout << "********Philospher " << id << " picked up chopstick #" << ((id+1)%numPhilosphers)<< endl;
    }
}

}

void
pickUpOtherS(state philospher)
{
  int id = philospher.value;
  if (numPhilosphers ==1)
  {
    cout << "*******Philospher " << id << " tries to pick up chopstick #" << id<< endl;
    (*(chopsticksS))->P();
    cout << "********Philospher " << id << " picked up chopstick #" << 0<< endl;

  }
  else
  {
    if ((id) % 2 ==0)
    {
      cout << "*******Philospher " << id << " tries to pick up chopstick #" << id<< endl;
      if (philosphers[(id+1)%numPhilosphers].EATING)
      {
        (*(chopsticksS+((id+1)%numPhilosphers)))->P();
        cout << "##########Avoided possible deadlock situation #######" << endl;
        (*(chopsticksS+((id+1)%numPhilosphers)))->V();

      }
      (*(chopsticksS+id))->P();

      cout << "********Philospher " << id << " picked up chopstick #" << id<< endl;

    }
    else{
      cout << "*******Philospher " << id << " tries to pick up chopstick #" << ((id+1)%numPhilosphers)<< endl;
      if (philosphers[(id+numPhilosphers-1)%numPhilosphers].EATING)
      {
        (*(chopsticksS+((id+numPhilosphers-1)%numPhilosphers)))->P();
        cout << "##########Avoided possible deadlock situation #######" << endl;
        (*(chopsticksS+((id+numPhilosphers-1)%numPhilosphers)))->V();

      }
      (*(chopsticksS+((id+1)%numPhilosphers)))->P();

      cout << "********Philospher " << id << " picked up chopstick #" << ((id+1)%numPhilosphers)<< endl;
    }
  }


}


void
putDownS(state philospher)
{
  int id = philospher.value;

  if (numPhilosphers ==1)
  {
    (*(chopsticksS+1))->V();
    cout << "**********Philospher " << id << " put down chopstick #" << 1<< endl;
  }
  else
  {
    if (id % 2 ==1)
    {
      (*(chopsticksS+((id)%numPhilosphers)))->V();
      cout << "**********Philospher " << id << " put down chopstick #" << id<< endl;

    }
    else
    {
      (*(chopsticksS+((id+1)%numPhilosphers)))->V();
      cout << "**********Philospher " << id << " put down chopstick #" << ((id+1)%numPhilosphers)<< endl;
    }
  }
}


void
putDownOtherS(state philospher)
{
  int id = philospher.value;
  if (numPhilosphers ==1)
  {
    (*(chopsticksS))->V();
    cout << "**********Philospher " << id << " put down chopstick #" << 0<< endl;
  }
  else
  {
    if (id % 2 ==0)
    {
      (*(chopsticksS+((id)%numPhilosphers)))->V();
      cout << "**********Philospher " << id << " put down chopstick #" << id<< endl;

    }
    else
    {
      (*(chopsticksS+((id+1)%numPhilosphers)))->V();
      cout << "**********Philospher " << id << " put down chopstick #" << ((id+1)%numPhilosphers)<< endl;
    }
  }
}

void
tableS(int id)
{
  cout << "*Philospher " << id << " has entered the room. " << endl;
  cout << "**Philospher " << id << " is waiting to sit at the table." <<endl;
  count++;
  if (count < numPhilosphers)
  {
    (*wait).P();
  }
  (*wait).V();




  if (id == (numPhilosphers-1))
  {
    cout << "***All philosphers has taken a seat!" << endl;
  }
  if (numPhilosphers == 1)
  {
    while (meals != 0)
    {
      cout << "****Philospher " << id << " is hungry." << endl;

      pickUpS(philosphers[0]);
      pickUpOtherS(philosphers[0]);
      meals-=1;
      cout << "*********Philospher " << id << " is eating." << " There are " << meals << " meals left."<< endl;

      for (int t = 0; t < (Random()%5+3);t++){}

      putDownS(philosphers[id]);
      putDownOtherS(philosphers[id]);

      cout << "************Philospher " << id << " is done eating and is now thinking." << endl;

      for (int y = 0; y < (Random()%5+3);y++){}

    }
  }
  else
  {
    while ((philosphers[id].remaining != 0))
    {
      philosphers[id].HUNGRY = true;

      cout << "****Philospher " << id << " is hungry." << endl;
      (*staring).P();

      pickUpS(philosphers[id]);
      pickUpOtherS(philosphers[id]);
      philosphers[id].HUNGRY = false;
      philosphers[id].EATING = true;
      philosphers[id].remaining-=1;
      meals++;
      cout << "*********Philospher " << id << " is eating." << " There are " << meals << " meals eaten."<< endl;
      if (meals==0){
        (*(chopsticksS+((id)%numPhilosphers)))->V();
        (*(chopsticksS+((id+1)%numPhilosphers)))->V();

      }
      (*staring).V();

      for (int t = 0; t < (Random()%5+3);t++)
      {
        currentThread -> Yield();
      }
      philosphers[id].EATING = false;
      putDownS(philosphers[id]);
      putDownOtherS(philosphers[id]);
      cout << "************Philospher " << id << " is done eating and is now thinking." << endl;
      philosphers[id].THINKING =true;

      for (int y = 0; y < (Random()%5+3);y++)
      {
        currentThread -> Yield();
      }
      philosphers[id].THINKING = false;

    }
    cout << "!!!!!!!!!Philospher " << id << " is waiting to leave." << endl;
  }
  leaving++;
  if (leaving == numPhilosphers){
    cout << "$$$$All philosphers have left the party!$$$" << endl;
  }
  else
  {
    (*wait).P();
  }
    (*wait).V();
}

void
diningPhilospherS(int which)
{
  cout << "How many philosphers will be at the dinner party? ";
  numPhilosphers = check();
  philosphers = new state[numPhilosphers];
  cout << "How many meals will be available? ";
  meals = check();
  Thread *t[numPhilosphers];


  if (numPhilosphers >1)
  {
    (chopsticksS) = new Semaphore*[numPhilosphers];
    for (int i =0; i<numPhilosphers;i++)
    {
      t[i] = new Thread("");
      *(chopsticksS+i) = new Semaphore("",1);
      philosphers[i].value = i;
      if (meals >0){
        philosphers[i].remaining++;
        meals--;
      }
    }
  }
  else
  {
    //chopsticks = new bool[2];
    t[0] = new Thread("");
    (chopsticksS) = new Semaphore*[2];
    for (int i=0;i<2;i++){
    *(chopsticksS+i) = new Semaphore("",1);
  }


  }
  for (int i = 0; i < numPhilosphers; i++)
  {
    t[i] -> Fork(tableS, i);
  }

}

//End code written by David Duong and Randy Nguyen

//Begin code written by Dan Truong and John Bui

//----------------------------------------------------------------------
// Reader
// 	This function contains the protocol for when readers attempt to 
//  enter the area. A maximum of readers is established and only that
//  number of readers are allowed at once. The readers protocol also
//  gives a writer permission to go and enter the area after the 
//  maximum is reached. Readers in the area will all leave at the same
//  time.
//----------------------------------------------------------------------

int currentReaders = 0;
int totalReads = 0;
int tempReaders = 1;
int currentWriters = 0;
int numWrite;
int numRead;
int maxRead;
int readerCount = 0;
Semaphore *area;
Semaphore *rdrCountCtrl;
Semaphore *writeCtrl;
Semaphore *readersIn;
Semaphore *waiter = new Semaphore("",0);

void
Reader(int which)
{
    (*readersIn).P();
    (*rdrCountCtrl).P();
    totalReads = totalReads +1;
    currentReaders = currentReaders +1;
    readerCount = readerCount +1;
    if (readerCount == 1)
    {
        (*area).P();
    }
    
    printf("-Reader %d is reading.\n",which);

    if((readerCount < maxRead))
    {
        (*rdrCountCtrl).V();
        if (totalReads == numRead)
        {
            goto skip;
        }
        (*waiter).P();
    }
    
    skip: ;
    
    printf("---Reader %d is leaving. Total reads : %d\n",which,tempReaders);
    readerCount = readerCount -1;
    tempReaders = tempReaders +1;
    if (readerCount == 0)
    {
        (*writeCtrl).V();
        (*rdrCountCtrl).V();
        (*area).V();
    }
    else
    {
        (*waiter).V();
    }
    if ((currentWriters==numWrite)&&(readerCount == 0))
    {
        (*area).P();
    }
}

//----------------------------------------------------------------------
// Writer
// 	Protocol for when a thread is forked as a writer.
//----------------------------------------------------------------------

void
Writer(int which)
{
    (*writeCtrl).P();
    (*area).P();
    printf("****Writer %d is writing.\n",which);
    printf("******Writer %d has finished writing.\n",which);

    readerCount = 0;
    
    for (size_t i = 0; i < currentReaders; i++)
    {
        (*readersIn).V();
    }
    currentReaders = 0;
    tempReaders = 1;
    currentWriters = currentWriters +1;
    (*area).V(); 
}

//----------------------------------------------------------------------
// readersWriters
// 	This function will take inputs from user to take a number of writers,
//  readers, and the maximum number of readers. Then it will fork the
//  respective number of threads for each reader and writer.
//----------------------------------------------------------------------

void
readersWriters(int which)
{
    char inputWrite[200];
    char inputRead[200];
    char inputMax[200];

    area = new Semaphore("",1);
    rdrCountCtrl = new Semaphore("",1);

    wrong: ;
    printf("Please enter an integer for number of writers (>0): ");
    scanf("%s", inputWrite);
    numWrite = atoi(inputWrite);
    if ((numWrite == 0)||(numWrite<0))
    {
        printf("Invalid input try again: ");
        goto wrong;
    }
    writeCtrl = new Semaphore("",0);
    
    wrong1: ;
    printf("Please enter an integer for number of readers (>0): ");
    scanf("%s", inputRead);
    numRead = atoi(inputRead);
    if ((numRead == 0)||(numRead<0))
    {
        printf("Invalid input try again: ");
        goto wrong1;
    }

    wrong2: ;
    printf("Please enter an integer for maximum number of readers at once (1 minimum and %d maximum): ",numRead);
    scanf("%s", inputMax);
    maxRead = atoi(inputMax);
    if ((maxRead == 0)||(maxRead<0))
    {
        printf("Invalid input try again: ");
        goto wrong2;
    }
    
    readersIn = new Semaphore("",maxRead);

    int totalEnt = numRead + numWrite;
    int nameWrite = 1;
    int nameRead = 1;

    for (size_t i = 0; i < numRead; i++)
    {
        Thread * boink = new Thread("");
        boink -> Fork(Reader, i);
    }

    for (size_t i = 0; i < numWrite; i++)
    {
        Thread * yaga = new Thread("");
        yaga -> Fork(Writer, i);
    }
}

//End code written by Dan Truong and John Bui

//Begin code written by Dan Truong and John Bui

//----------------------------------------------------------------------
// Post Office problem
// 	
//----------------------------------------------------------------------

int participating;
int capacity =0;
int maxMessages =0;
Semaphore **mailbox;
int leave=0;
bool sending = true;
Semaphore *rdrctrl = new Semaphore("",1);
struct person {
    int mail = 0;
    int send = 0;
    bool fullBox =false;
    int waitering =0;
};

struct message {
    int id = -1;
    char msg [200];
};
message **mailboxes;
person *people;

//----------------------------------------------------------------------
// readMessages
//  This function is call by the participants to read their messages.
//----------------------------------------------------------------------

void 
readMessages(int which)
{
    yaga: ;
    for (size_t i = 0; i < capacity; i++)
    {
        if (people[which].mail == 0)
        {
            printf("** Person %d has no messages.\n",which);
            currentThread->Yield();
            break;
        }
        if (mailboxes[which][i].id != -1)
        {
            (*(mailbox+which))->V();
        printf("*** Person %d reads message: %s from Person %d\n",which, mailboxes[which][i].msg,mailboxes[which][i].id);
        mailboxes[which][i].id = -1;
        people[which].mail--;
        (*rdrctrl).P();
        totalReads = totalReads +1;
        (*rdrctrl).V();
        printf("**** Person %d is now yielding.\n",which);
        currentThread->Yield();
        }
    }
    if (people[which].mail!=0)
    {
        goto yaga;
    } 
}

//----------------------------------------------------------------------
// writeMessage
//  This function attempts to send a message to a random participant
//  and if successful, generates a message and places it into the 
//  person's mailbox with an ID.
//----------------------------------------------------------------------

void 
writeMessage(int which)
{
    char* shoutList [] = {"ding dong","yoink","Everyone dies, some just need a little help.",
    "By the light","yaga"};

    int sendTo = which;
    while (sendTo == which)
    {
        sendTo = Random()%participating;
    }
    tryagain: ;
    printf("***** Person %d tried to send a message to person %d.\n",which, sendTo);
    if (people[sendTo].mail==capacity)
    {
        printf("****** Person %d failed to send message to person %d.\n",which,sendTo);
        currentThread->Yield();
        if (people[sendTo].waitering ==3)
        {
            printf("!!!!! Person %d avoided a potential deadlock and did not send a message!\n", which);
            people[sendTo].waitering=0;
            goto avoid;
        }
        people[sendTo].waitering++;
        goto tryagain;
    }
    
    (*(mailbox+sendTo))->P();
    for (size_t i = 0; i < capacity; i++)
    {
        if (mailboxes[sendTo][i].id==-1)
        {
            strcpy(mailboxes[sendTo][i].msg, shoutList[Random()%5]);
            mailboxes[sendTo][i].id = which;
            people[sendTo].mail++;
            printf("******* Person %d sent message to person %d in mailbox %d.\n",which,sendTo,i);
            people[which].send--;
            break;
        }
    }
    avoid: ;
}

//End code written by Dan Truong and John Bui

//Begin code written by David Duong and Randy Nguyen

//----------------------------------------------------------------------
// enterOffice
//  This function is the functions that the threads, or participants,
//  are forked to. This is where they enter the post office and decide 
//  what they will do.
//----------------------------------------------------------------------

void 
enterOffice(int which)
{
    while (true)
    {
        printf("* Person %d has entered the post office.\n",which);
        readMessages(which);
        if ((totalReads == maxMessages))
        {
            break;
        }
            
        
        if (people[which].send!=0){
            writeMessage(which);
        }
        printf("******** Person %d is leave the post office.\n",which);
        int var = Random()%5+3;
        printf("********* Person %d yields %d cycle(s).\n",which, var);

        for (size_t i = 0; i < var; i++)
        {
            currentThread->Yield();
        } 
        
        
    }
    (*rdrctrl).P();
    leave++;
    (*rdrctrl).V();
    printf("******* Person %d is free to go home and never come back! *********\n",which);

    if (participating == leave){
        printf("$$$$$$$$ All %d messages have been sent and read! $$$$$$$$\n",maxMessages);
        printf("$$$$$$$$ All %d  people have left the post office! $$$$$$$$\n",participating);
        
    }
}

//----------------------------------------------------------------------
// enterOfficeGo
//  The starting function to fork to that will take user inputs for the 
//  number of participants, maximum capacity of the mailboxes, and 
//  the maximum messages to send.
//----------------------------------------------------------------------

int leftover = 0;
void 
enterOfficeGo(int which){
    cout<< "Please enter the amount of people participating: ";
    cin >> participating;
    people = new person[participating];
    mailboxes = new message*[participating];
    //mailboxes = new string*[participating];
    cout << "Please enter the capacity for the mailboxes: ";
    cin >> capacity;
    mailbox = new Semaphore*[participating];
    for (int i =0; i < participating; i++ )
    {
        *(mailboxes+i) = new message[capacity];
        *(mailbox+i)=new Semaphore("",capacity);
    }
    // for (int i =0; i < participating;i++)
    // {
    //     for (int y =0;y<capacity;y++)
    //     {
    //         *(*(mailboxes+i)+y);
    //     }
    // }
    cout<< "Please enter the total number of messages to be sent: ";
    cin >> maxMessages;
    if (maxMessages % participating !=0)
    {
        leftover = maxMessages % participating;
        for (size_t i = 0; i < participating; i++)
        {
            people[i].send = floor(maxMessages/participating);
            if (leftover!=0)
            {
                people[i].send = people[i].send + 1;
                leftover = leftover -1;
            }
        }
    }
    else
    {
        for (size_t i = 0; i < participating; i++)
        {
            people[i].send = maxMessages/participating;
        }
    }

    for (size_t i = 0; i < participating; i++)
    {
        Thread * boink = new Thread("");
        boink -> Fork(enterOffice, i);
    }
}

//End code written by David Duong and Randy Nguyen

//----------------------------------------------------------------------
// ThreadTest
// 	Invoke a test routine.
//----------------------------------------------------------------------

void
ThreadTest()
{
    DEBUG('t', "Entering ThreadTest");
    //Begin code changes by Dan Truong

    if (variable == 1)
    {
        Thread *chaeng = new Thread("inputValidate");
        chaeng->Fork(InputValidation, 1);
    }
    else if (variable == 2)
    {
        Thread *bing = new Thread("ShoutTest");
        bing->Fork(ShoutTest,1);
    }
    else if (variable == 3)
    {
        Thread *bing = new Thread("philosopherBusy");
        bing->Fork(diningPhilospher,1);
    }
    else if (variable == 4)
    {
        Thread *bing = new Thread("philosopherSem");
        bing->Fork(diningPhilospherS,1);
    }
    else if (variable == 5)
    {
        Thread *bing = new Thread("enterOfficeGo");
        bing->Fork(enterOfficeGo,1);
    }
    else if (variable == 6)
    {
        Thread *bing = new Thread("readerWriters");
        bing->Fork(readersWriters,1);
    }

    //End code changes by Dan Truong
}

